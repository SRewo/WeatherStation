﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WeatherStation.App
{
    internal static partial class AppApiKeys
    {
        public static readonly string AccuWeatherApiKey = "";
        public static readonly string PositionstackApiKey = "";
        public static readonly string WeatherbitApiKey = "";
    }
}
